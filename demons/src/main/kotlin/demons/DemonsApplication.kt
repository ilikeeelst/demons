package demons

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemonsApplication

fun main(args: Array<String>) {
    runApplication<DemonsApplication>(*args)
}
